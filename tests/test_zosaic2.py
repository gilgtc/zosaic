# -*- coding: utf-8 -*-
"""
Created on Fri Nov 24 18:11:26 2017

@author: choo
"""

from keras import backend as K
from zosaic import Zosaic
import dill as pickle

perplexity = 40
files_path = r'C:/Users/choo/Documents/Python Scripts/images'
files_path = r'C:/Users/choo/Documents/Python Scripts/james/images'

reduction_models = ['tnse','pca','clusters']
cluster_models = ['kmeans', 'dbscan', 'hdbscan', 'affinitypropagation',
                 'meanshift', 'spectralclustering', 'agglomerativeclustering']
cm = cluster_models[0]
rm = reduction_models[2]
rm = 'tsne'
mosaic_file_basic = 'mosaic_james'
zo = Zosaic(files_path, perplexity = 40)
zo.load_vgg16()
#zo.load_vgg19()
zo.run_model(1,0)
#zo.clusters(cm)
#zo.build_cluster_mosaic(**{'shape': 'rectangle', 'filename': 'pai.png','cluster_mosaic_filename':'cluster_mosaic.jpg'})
shape = 'image'
mosaic_filename = '%s_%d_%s_%s.jpg' % (mosaic_file_basic, perplexity, zo.model_name, shape)
parameters = {'shape': shape, 'filename': 'moustache.jpg','mosaic_filename':mosaic_filename}
zo.build_mosaic(**parameters)
# if rm == 'tnse':
#     #perplexities = [5,10,15,20,25,30,35,40,45,50,55,60,65]
#     perplexities = [40]
#     for num, perplexity in enumerate(perplexities):
#         print('------ On perplexity %d of %d (perplexity = %d) ------' % (num + 1,
#               len(perplexities), perplexity))
#         zo.run_tsne(perplexity = perplexity)
#         mosaic_file = '%s_%d_%s.jpg' % (mosaic_file_basic, perplexity, zo.model_name)
#         zo.build_mosaic(**{'shape': 'image', 'filename': 'pai.png','mosaic_filename':'mosaic.jpg'})
#
# elif rm == 'pca':
#     mosaic_file = '%s_%s_%s.jpg' % (mosaic_file_basic, rm, zo.model_name)
#     zo.run_pca()
#     zo.build_mosaic(**{'mosaic_filename':'mosaic.jpg'})
#
# elif rm == 'clusters':
#     zo.run_pca()
#     for cm in cluster_models:
#         zo.clusters(cm)

#with open('predictions.dat', 'wb') as f:
    #pickle.dump(zo.predictions, f)
