======
zosaic
======


.. image:: https://img.shields.io/pypi/v/zosaic.svg
        :target: https://pypi.python.org/pypi/zosaic

.. image:: https://img.shields.io/travis/gilgtc/zosaic.svg
        :target: https://travis-ci.org/gilgtc/zosaic

.. image:: https://readthedocs.org/projects/zosaic/badge/?version=latest
        :target: https://zosaic.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status

.. image:: https://pyup.io/repos/github/gilgtc/zosaic/shield.svg
     :target: https://pyup.io/repos/github/gilgtc/zosaic/
     :alt: Updates


Image mosaic derived from a CNN with the VGG16 network and dimension reduction with t-SNE.

For more details find a poster and a report on this project below:

Report: http://cs229.stanford.edu/proj2017/final-reports/5237777.pdf

Poster: http://cs229.stanford.edu/proj2017/final-posters/5140123.pdf


* Free software: MIT license
* Documentation: https://zosaic.readthedocs.io.


Features
--------

* TODO

Credits
---------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage

