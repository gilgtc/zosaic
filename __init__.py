# -*- coding: utf-8 -*-

"""Top-level package for zosaic."""

__author__ = """Goncalo Gil\"""
__email__ = 'gilg@stanford.edu'
__version__ = '0.1.0'
