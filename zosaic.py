# -*- coding: utf-8 -*-

"""Main module."""

import matplotlib.pyplot as plt
from keras.applications.vgg16 import VGG16
from keras.applications.vgg19 import VGG19
from keras.preprocessing import image as image_utils
from keras.models import Model
from keras.applications.vgg16 import preprocess_input, decode_predictions
import numpy as np
import glob
from random import randint
import tensorflow as tf
from sklearn.manifold import TSNE
import os
from PIL import Image, ImageFont, ImageDraw
import rasterfairy
from sklearn import decomposition
import hdbscan
import time
import seaborn as sns
import sklearn.cluster as cluster
import _pickle as pickle
import random

class Zosaic:

    def __init__(self, img_folder, perplexity = 40, fraction = 1.):

        # Configure tensorflow
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        self.sess = tf.Session(config = config)
        self.img_folder = img_folder
        self.img_list_path = np.array(glob.glob(os.path.join(self.img_folder,'*')))
        N = len(self.img_list_path)
        if fraction < 1.:
            idx = random.sample(range(0, N-1), np.int(fraction*N))
            self.img_list_path = self.img_list_path[idx]
            N = len(self.img_list_path)
        print('There are %d files total.' % N)
        self.M = len(self.img_list_path)
        self.width = 200
        self.height = 200
        self.use_clusters = False
        self.perplexity = perplexity

    def load_vgg19(self):
        print('Loading VGG19 network...')
        self.model = VGG19(weights='imagenet', include_top=True)
        self.model_fc1 = Model(input=self.model.input, output=self.model.get_layer('fc1').output)
        self.model_name = 'VGG19'
        print('Done...')

    def load_vgg16(self):
        print('Loading VGG16 network...')
        self.model = VGG16(weights='imagenet', include_top=True)
        self.model_fc1 = Model(input=self.model.input, output=self.model.get_layer('fc1').output)
        self.model_name = 'VGG16'
        print('Done...')

    def clusters(self, cluster_model):
        self.cluster_data = self.X_2d
        data = self.cluster_data
        if cluster_model == 'kmeans':
            print('Computing clusters (K-Means)...')
            #plot_clusters(data, cluster.KMeans, (), {'n_clusters':3})
            self.labels = cluster.KMeans(n_clusters=3, random_state=0).fit_predict(data)
            print('Done...')

        elif cluster_model == 'hdbscan':
            print('Computing clusters (HDBSCAN)...')
            plot_clusters(data, hdbscan.HDBSCAN, (), {'min_cluster_size':15})
            print('Done...')

        elif cluster_model == 'affinitypropagation':
            print('Computing clusters (Affinity Propagation)...')
            plot_clusters(data, cluster.AffinityPropagation, (), {'preference':-5.0, 'damping':0.95})
            print('Done...')

        elif cluster_model == 'meanshift':
            print('Computing clusters (Mean Shift)...')
            plot_clusters(data, cluster.MeanShift, (0.175,), {'cluster_all':False})
            print('Done...')

        elif cluster_model == 'spectralclustering':
            print('Computing clusters (Spectral Clustering)...')
            plot_clusters(data, cluster.SpectralClustering, (), {'n_clusters':3})
            self.labels = cluster.SpectralClustering(n_clusters=3, random_state=0).fit_predict(data)
            print('Done...')

        elif cluster_model == 'agglomerativeclustering':
            print('Computing clusters (Agglomerative Clustering)...')
            plot_clusters(data, cluster.AgglomerativeClustering, (), {'n_clusters':6, 'linkage':'ward'})
            print('Done...')

        elif cluster_model == 'dbscan':
            print('Computing clusters (DBSCAN)...')
            plot_clusters(data, cluster.DBSCAN, (), {'eps':0.025})
            print('Done...')

    def run_model(self, save = 0, load = 0):

        self.img_list = []
        X = np.zeros((self.M,224,224,3))
        print('Processing images...')
        for m in range(self.M):
            #print(r'On image %d/%d.' % (m+1, M))
            img_path = self.img_list_path[m]
            img = image_utils.load_img(img_path, target_size=(224, 224))
            x = image_utils.img_to_array(img)
            X[m,:,:,:] = x
            self.img_list.append(Image.open(img_path))

        X = preprocess_input(X)
        print('Done...')
        print('Extracting features (%s)...' % self.model_name)
        self.features_1000 = self.model.predict(X)
        self.features_4096 = self.model_fc1.predict(X)
        print('Done...')
        print('Decoding predictions (%s)...' % self.model_name)
        self.predictions = decode_predictions(self.features_1000)
        print('Done...')

        self._run_tsne(self.perplexity)

        if save:
            print('Saving predictions to disk (%s)...' % self.model_name)
            with open('labels.pickle', 'wb') as handle:
                pickle.dump(self.features_1000, handle)
            print('Done...')

    def _run_tsne(self, perplexity = 5):
        self.perplexity = perplexity
        print('Reducing dimensions of feature vector (t-SNE with perplexity = %d)...' % self.perplexity)
        self.X_2d = TSNE(n_components=2, init='pca', random_state=0,
                    perplexity = self.perplexity).fit_transform(self.features_4096)
        print('Done...')

    def _run_pca(self):
        print('Reducing dimensions of feature vector (PCA)...')
        pca = decomposition.PCA(n_components=2)
        self.X_2d = pca.fit_transform(self.features_4096)
        print('Done...')

    def _compute_2dgrid(self, **kwargs):
        M = self.M
        X_2d = self.X_2d
        print('Computing grid (rasterfairy)...')
        # image mask
        if kwargs['shape'] == 'image':
            import PIL.Image as PImage
            img = PImage.open(kwargs['filename'])
            arrangement = rasterfairy.getRasterMaskFromImage(img)

        elif kwargs['shape'] == 'circle':
            radius,adjustmentFactor,count = rasterfairy.getBestCircularMatch(M)
            arrangement = rasterfairy.getCircularArrangement(radius,adjustmentFactor)
            arrangement = rasterfairy.arrangementToRasterMask(arrangement)

        elif kwargs['shape'] == 'rectangle':
            arrangement = rasterfairy.getRectArrangements(M)[0]

        elif kwargs['shape'] == 'triangle':
            #other arrangements:
            arrangements = rasterfairy.getArrangements(M)
            #...which have to be converted to a raster mask
            arrangement = rasterfairy.arrangementListToRasterMasks(arrangements)[1]

        self.grid_xy, (self.rows, self.columns) = rasterfairy.transformPointCloud2D(X_2d, target=arrangement)

        print('Grid dimensions: ' + str(self.rows) + ' x ' + str(self.columns))
        print('Done...')

    def build_cluster_mosaic(self, **kwargs):
        self._compute_2dgrid(**kwargs)
        print('Building cluster mosaic...')
        H = self.rows * self.height
        W = self.columns * self.width
        print(H,W)
        E = int(max(H,W))
        mosaic = Image.new( 'RGB', (H+E, W+E))
        draw = ImageDraw.Draw(mosaic)
        N_clusters = len(np.unique(self.labels))
        for i in range(N_clusters):
            print('In cluster %d, %d photos.' % (i+1, N_clusters))
            idx = np.where(self.labels == i)[0]
            theta = i*np.pi/2.
            dx = np.cos(theta)*E
            dy = np.sin(theta)*E
            for j in idx:
                # paste the image at grid_xy location:
                img_resized = resize_and_crop(self.img_list[j], (self.width, self.height))
                mosaic.paste(img_resized, (int((self.grid_xy[j,0])*self.width+dx),
                                           int((self.grid_xy[j,1])*self.height+dy)))
                # render text at grid_xy location:
                draw.text((int((self.grid_xy[j,0])*self.width+dx),
                           int((self.grid_xy[j,1])*self.height+dy)), '%s' % self.predictions[j][0][1])
                # Select next image and text

        print('Saving cluster mosaic file to %s...' % kwargs['cluster_mosaic_filename'])
        mosaic.save(kwargs['cluster_mosaic_filename'], quality=90, optimize=True, progressive=True)
        print('Done...')

    def build_mosaic(self, **kwargs):
        self._compute_2dgrid(**kwargs)
        print('Building mosaic...')
        img_list_rz = []
        for img in self.img_list:
            # resize images
            img_resized = resize_and_crop(img,(self.width, self.height))
            img_list_rz.append(img_resized)

        mosaic = Image.new( 'RGB', (self.rows * self.height,
                                    self.columns * self.width))

        # Prepare draw and font objects to render text
        draw = ImageDraw.Draw(mosaic)

        for i in range(self.M):
            # paste the image at location i,j:

            mosaic.paste(img_list_rz[i], (int((self.grid_xy[i,0])*self.width),
                                          int((self.grid_xy[i,1])*self.height)))
            # render text at location i,j:
            draw.text((int((self.grid_xy[i,0])*self.width),
                       int((self.grid_xy[i,1])*self.height)), '%s' % self.predictions[i][0][1])
            # Select next image and text

        print('Saving mosaic file to %s...' % kwargs['mosaic_filename'])
        mosaic.save(kwargs['mosaic_filename'], quality=90, optimize=True, progressive=True)
        print('Done...')

def resize_and_crop(img, size, crop_type = 'middle'):
    """
    Resize and crop an image to fit the specified size.

    args:
    img_path: path for the image to resize.
    modified_path: path to store the modified image.
    size: `(width, height)` tuple.
    crop_type: can be 'top', 'middle' or 'bottom', depending on this
    value, the image will cropped getting the 'top/left', 'middle' or
    'bottom/right' of the image to fit the size.
    raises:
    Exception: if can not open the file in img_path of there is problems
    to save the image.
    ValueError: if an invalid `crop_type` is provided.
    """
    # If height is higher we resize vertically, if not we resize horizontally
    #img = Image.open(img_path)
    # Get current and desired ratio for the images

    img_ratio = img.size[0] / float(img.size[1])
    ratio = size[0] / float(size[1])
    #The image is scaled/cropped vertically or horizontally depending on the ratio
    if ratio > img_ratio:
        img = img.resize((size[0], int(round(size[0] * img.size[1] / img.size[0]))),
            Image.ANTIALIAS)
        # Crop in the top, middle or bottom
        if crop_type == 'top':
            box = (0, 0, img.size[0], size[1])
        elif crop_type == 'middle':
            box = (0, int(round((img.size[1] - size[1]) / 2)), img.size[0],
                int(round((img.size[1] + size[1]) / 2)))
        elif crop_type == 'bottom':
            box = (0, img.size[1] - size[1], img.size[0], img.size[1])
        else :
            raise ValueError('ERROR: invalid value for crop_type')
        img = img.crop(box)
    elif ratio < img_ratio:
        img = img.resize((int(round(size[1] * img.size[0] / img.size[1])), size[1]),
            Image.ANTIALIAS)
        # Crop in the top, middle or bottom
        if crop_type == 'top':
            box = (0, 0, size[0], img.size[1])
        elif crop_type == 'middle':
            box = (int(round((img.size[0] - size[0]) / 2)), 0,
                int(round((img.size[0] + size[0]) / 2)), img.size[1])
        elif crop_type == 'bottom':
            box = (img.size[0] - size[0], 0, img.size[0], img.size[1])
        else :
            raise ValueError('ERROR: invalid value for crop_type')
        img = img.crop(box)
    else :
        img = img.resize((size[0], size[1]),
            Image.ANTIALIAS)
    # If the scale is the same, we do not need to crop
    #img.save(modified_path)
    return img

def plot_clusters(data, algorithm, args, kwargs):
    sns.set_context('poster')
    sns.set_color_codes()
    plot_kwargs = {'alpha' : 0.25, 's' : 80, 'linewidths':0}
    start_time = time.time()
    labels = algorithm(*args, **kwargs).fit_predict(data)
    end_time = time.time()
    palette = sns.color_palette('deep', np.unique(labels).max() + 1)
    colors = [palette[x] if x >= 0 else (0.0, 0.0, 0.0) for x in labels]
    #pca = decomposition.PCA(n_components=2)
    #data = pca.fit_transform(data)
    print('Running t-SNE on cluster data.')
    data = TSNE(n_components=2, init='pca', random_state=0,
                perplexity = 30).fit_transform(data)
    plt.scatter(data[:,0], data[:,1], c=colors, **plot_kwargs)
    frame = plt.gca()
    frame.axes.get_xaxis().set_visible(False)
    frame.axes.get_yaxis().set_visible(False)
    plt.title('Clusters found by {}'.format(str(algorithm.__name__)), fontsize=24)
    plt.text(-0.5, 0.7, 'Clustering took {:.2f} s'.format(end_time - start_time), fontsize=14)
    plt.show()
